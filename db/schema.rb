# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_04_18_051533) do
  create_table "reward_categories", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rewards", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status", default: "Active"
    t.string "category"
    t.index ["user_id"], name: "index_rewards_on_user_id"
  end

  create_table "tiers", force: :cascade do |t|
    t.integer "min_points", default: 0
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.string "category", null: false
    t.string "entry_type", default: "Ledger", null: false
    t.decimal "amount", null: false
    t.integer "user_id", null: false
    t.bigint "parent_id"
    t.date "record_date", null: false
    t.date "expire_date"
    t.boolean "international", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status", default: "Active"
    t.decimal "amount_for_cashback"
    t.index ["parent_id"], name: "index_transactions_on_parent_id"
    t.index ["user_id"], name: "index_transactions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.date "date_of_birth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "wallet_id", null: false
    t.integer "tier_id", null: false
    t.string "level", default: "Level 1"
    t.index ["name"], name: "index_users_on_name", unique: true
    t.index ["tier_id"], name: "index_users_on_tier_id"
    t.index ["wallet_id"], name: "index_users_on_wallet_id"
  end

  create_table "wallets", force: :cascade do |t|
    t.integer "point", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "rewards", "users"
  add_foreign_key "transactions", "users"
  add_foreign_key "users", "tiers"
  add_foreign_key "users", "wallets"
end

class AddAmountForCashback < ActiveRecord::Migration[7.1]
  def change
    add_column :transactions, :amount_for_cashback, :decimal
  end
end

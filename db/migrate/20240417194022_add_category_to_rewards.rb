class AddCategoryToRewards < ActiveRecord::Migration[7.1]
  def change
    add_column :rewards, :category, :string
    change_column_default :rewards, :status, 'Active'
  end
end

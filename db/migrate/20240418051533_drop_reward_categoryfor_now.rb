class DropRewardCategoryforNow < ActiveRecord::Migration[7.1]
  def change
    # NOTE: Time Constraint so dropping foreign key
    remove_reference :rewards, :reward_category, foreign_key: true
  end
end

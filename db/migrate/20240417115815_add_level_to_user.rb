class AddLevelToUser < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :level, :string, default: 'Level 1'
  end
end

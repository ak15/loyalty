class CreateTransactions < ActiveRecord::Migration[7.1]
  def change
    create_table :transactions do |t|
      t.string :category, null: false
      t.string :entry_type, null: false, default: 'Ledger'
      t.decimal :amount, null: false
      t.references :user, null: false, foreign_key: true
      t.bigint :parent_id, null: true, index: true
      t.date :record_date, null: false
      t.date :expire_date
      t.boolean :international, default: false

      t.timestamps
    end
  end
end

class AddStatusToRewardsAndTransactions < ActiveRecord::Migration[7.1]
  def change
    add_column :rewards, :status, :string, default: 'Active'
    add_column :transactions, :status, :string, default: 'Active'
  end
end

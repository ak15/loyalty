class CreateRewardCategories < ActiveRecord::Migration[7.1]
  def change
    create_table :reward_categories do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end

class CreateTiers < ActiveRecord::Migration[7.1]
  def change
    create_table :tiers do |t|
      t.integer :min_points, default: 0
      t.string :name, null: false

      t.timestamps
    end
    # NOTE: We can have refactoring here to use some library to seed data once in the future or some admin dashboard
    Tier.create(name: 'Standard')
    Tier.create(name: 'Gold', min_points: 1000)
    Tier.create(name: 'Platinum', min_points: 5000)
  end
end

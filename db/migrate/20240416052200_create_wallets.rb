class CreateWallets < ActiveRecord::Migration[7.1]
  def change
    create_table :wallets do |t|
      t.integer :point, default: 0, null: false

      t.timestamps
    end
  end
end

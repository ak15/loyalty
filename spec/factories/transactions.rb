FactoryBot.define do
  factory :transaction do
    category { 'external' }
    amount { Faker::Number.decimal(l_digits: 2) }
    user { nil }
    parent { nil }
    record_date { Faker::Date.between(from: Date.today + 1, to: Date.today + 4) }
    expire_date { Faker::Date.between(from: Date.today + 1, to: Date.today + 4) }
    international { false }

    trait :internal_transaction do
      category { 'internal' }
      entry_type { 'ledger' }
      status { 'active' }
    end

    trait :external_transaction do
      category { 'external' }
      entry_type { 'ledger' }
      status { 'active' }
    end
  end
end

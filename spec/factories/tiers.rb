FactoryBot.define do
  factory :tier do
    min_points { 0 }
    name { 'Standard' }
  end
end

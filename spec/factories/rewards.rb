FactoryBot.define do
  factory :reward do
    user { nil }
    reward_category { nil }
    count { 1 }
  end
end

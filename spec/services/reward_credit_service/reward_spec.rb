require 'rails_helper'

RSpec.describe RewardCreditService::Reward do
  describe '#execute' do
    let(:user) { create(:user) }
    it 'calls the level specific reward method' do
      reward = described_class.new(user)
      expect(reward).to receive(:level_1_reward)
      reward.execute
    end

    it 'does not call the level specific reward method' do
      reward = described_class.new(user)
      user.update(level: 'level_2')
      expect(reward).not_to receive(:level_1_reward)
      reward.execute
    end
  end

  describe '#level_1_reward' do
    let(:user) { create(:user, level: 'Level 1') }

    context 'transaction without specified points or within month' do
      it 'does not credit reward' do
        create(:transaction, :internal_transaction, amount: 100, user: user, record_date: Date.today - 60.days)
        PointAllocationService::Cashback.new(user).execute
        described_class.new(user).level_1_reward
        expect(user.rewards.free_coffee.count).to eq(0)
      end

      it 'does not credit reward' do
        create(:transaction, :internal_transaction, amount: 100, user: user,
                                                    record_date: Date.today)
        PointAllocationService::Cashback.new(user).execute
        described_class.new(user).level_1_reward
        expect(user.rewards.free_coffee.count).to eq(0)
      end
    end

    context 'creates transaction with specified reward amount and month' do
      before do
        create(:transaction, :internal_transaction, amount: 1000,
                                                    user: user, record_date: Date.today)
        PointAllocationService::Cashback.new(user).execute
      end

      it 'creates a reward transaction and credits user wallet' do
        described_class.new(user).level_1_reward
        expect(user.rewards.free_coffee.count).to eq(1)
        described_class.new(user).level_1_reward
      end

      it 'checks idempotent' do
        described_class.new(user).level_1_reward
        expect(user.rewards.free_coffee.count).to eq(1)
        described_class.new(user).level_1_reward
        expect(user.rewards.free_coffee.count).to eq(1)
      end
    end
  end

  describe '#level_2_reward' do
    let(:user) { create(:user, level: 'Level 2') }

    context 'birthday month reward' do
      it 'creates a reward transaction and gives reward' do
        user.update(date_of_birth: user.date_of_birth.change(month: Date.today.month))
        described_class.new(user).level_2_reward
        expect(user.rewards.birthday_reward_coffee.count).to eq(1)
      end

      it 'is idempotent' do
        user.update(date_of_birth: user.date_of_birth.change(month: Date.today.month))
        described_class.new(user).level_2_reward
        expect(user.rewards.birthday_reward_coffee.count).to eq(1)
        described_class.new(user).level_2_reward
        expect(user.rewards.birthday_reward_coffee.count).to eq(1)
      end
    end

    context 'cashback rebate 5 percent reward' do
      it 'creates a reward transaction and gives reward' do
        create_list(:transaction, 10, :internal_transaction, amount: 150, user: user)
        described_class.new(user).level_2_reward
        expect(user.rewards.cashback_rebate_5_percent.count).to eq(1)
      end

      it 'does not give reward for less than 10 transaction' do
        create_list(:transaction, 9, :internal_transaction, amount: 150, user: user)
        described_class.new(user).level_2_reward
        expect(user.rewards.cashback_rebate_5_percent.count).to eq(0)
      end

      it 'does not gives reward for amount less than 100' do
        create_list(:transaction, 10, :internal_transaction, amount: 90, user: user)
        described_class.new(user).level_2_reward
        expect(user.rewards.cashback_rebate_5_percent.count).to eq(0)
      end

      it 'is idempotent' do
        create_list(:transaction, 10, :internal_transaction, amount: 150, user: user)
        described_class.new(user).level_2_reward
        expect(user.rewards.cashback_rebate_5_percent.count).to eq(1)
        described_class.new(user).level_2_reward
        expect(user.rewards.cashback_rebate_5_percent.count).to eq(1)
      end
    end

    context 'free movie tickets reward' do
      it 'creates a reward transaction and gives reward for more than 1000 in exact time frame' do
        create(:transaction, :external_transaction, amount: 500, user: user, record_date: Date.today - 30.days)
        create(:transaction, :external_transaction, amount: 500, user: user, record_date: Date.today)
        described_class.new(user).level_2_reward
        expect(user.rewards.free_movie_tickets.count).to eq(1)
      end

      it 'does not give reward for amount less than 1000 in exact time frame' do
        create(:transaction, :external_transaction, amount: 500, user: user, record_date: Date.today - 30.days)
        create(:transaction, :external_transaction, amount: 400, user: user, record_date: Date.today)
        described_class.new(user).level_2_reward
        expect(user.rewards.free_movie_tickets.count).to eq(0)
      end

      it 'does not give reward for amount more than 1000 beyond time frame' do
        create(:transaction, :external_transaction, amount: 500, user: user, record_date: Date.today - 70.days)
        create(:transaction, :external_transaction, amount: 500, user: user, record_date: Date.today)
        described_class.new(user).level_2_reward
        expect(user.rewards.free_movie_tickets.count).to eq(0)
      end
    end

    context 'quarterly reward' do
      it 'credits reward for milestone achieved' do
        create_list(:transaction, 10, :internal_transaction, amount: 1500, user: user, record_date: Date.today)
        described_class.new(user).level_2_reward
        expect(user.rewards.quarterly_bonus_milestone.count).to eq(1)
      end

      it 'does not credit reward for milestone not achieved' do
        create_list(:transaction, 1, :internal_transaction, amount: 10, user: user, record_date: Date.today)
        described_class.new(user).level_2_reward
        expect(user.rewards.quarterly_bonus_milestone.count).to eq(0)
      end
    end

    context 'gold tier reward' do
      it 'credits reward for milestone achieved' do
        user.update(tier_id: Tier.gold_rule.id)
        described_class.new(user).level_2_reward
        expect(user.rewards.airport_longue_four_tickets.count).to eq(1)
      end

      it 'does not credit reward for milestone not achieved' do
        create_list(:transaction, 1, :internal_transaction, amount: 10, user: user, record_date: Date.today)
        described_class.new(user).level_2_reward
        expect(user.rewards.airport_longue_four_tickets.count).to eq(0)
      end
    end
  end
end

require 'rails_helper'

RSpec.describe PointAllocationService::Cashback do
  describe '#execute' do
    let(:user) { create(:user) }
    it 'calls the level specific cashback method' do
      cashback = described_class.new(user)
      expect(cashback).to receive(:level_1_cashback)
      cashback.execute
    end

    it 'does not call the level specific cashback method' do
      cashback = described_class.new(user)
      user.update(level: 'level_2')
      expect(cashback).not_to receive(:level_1_cashback)
      cashback.execute
    end
  end

  describe '#level_1_cashback' do
    let(:user) { create(:user, level: 'Level 1') }

    context 'single transaction adds point to wallet' do
      let(:transaction) { create(:transaction, :internal_transaction, amount: 100, user: user) }
      it 'creates a cashback transaction and credits user wallet' do
        described_class.new(transaction.user).level_1_cashback
        expect(user.wallet.point).to eq(10)
      end
    end

    context 'multipe transactions and adds calculated point to wallet' do
      before do
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
      end

      it 'creates a cashback transaction and credits user wallet' do
        described_class.new(user).level_1_cashback
        expect(user.wallet.point).to eq(60)
      end
    end

    context 'multipe transaction and cashback calculated multiple time to proove it is idempotent' do
      before do
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
      end
      it 'creates a cashback transaction and credits user wallet' do
        described_class.new(user).level_1_cashback
        expect(user.wallet.point).to eq(60)
        described_class.new(user).level_1_cashback
        expect(user.wallet.point).to eq(60)
      end
    end

    context 'multipe transaction and add single transaction in next statement' do
      before do
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
      end
      it 'creates a cashback transaction and credits user wallet' do
        described_class.new(user).level_1_cashback
        expect(user.wallet.point).to eq(80)
        create(:transaction, :external_transaction, amount: 150, user: user)
        described_class.new(user).level_1_cashback
        user.reload
        expect(user.wallet.point).to eq(90)
      end
    end
  end

  describe '#level_2_cashback' do
    let(:user) { create(:user, level: 'Level 2') }

    context 'single transaction adds point to wallet' do
      let(:transaction) { create(:transaction, :internal_transaction, amount: 100, user: user) }
      it 'creates a cashback transaction and credits user wallet' do
        described_class.new(transaction.user).level_2_cashback
        expect(user.wallet.point).to eq(10)
      end
    end

    context 'multipe transactions and adds calculated point to wallet' do
      before do
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
      end

      it 'creates a cashback transaction and credits user wallet' do
        described_class.new(user).level_2_cashback
        expect(user.wallet.point).to eq(50)
      end
    end

    context 'multipe transaction and cashback calculated multiple time to proove it is idempotent' do
      before do
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
      end
      it 'creates a cashback transaction and credits user wallet' do
        described_class.new(user).level_2_cashback
        expect(user.wallet.point).to eq(50)
        described_class.new(user).level_2_cashback
        expect(user.wallet.point).to eq(50)
      end
    end

    context 'multipe transaction and add single transaction in next statement' do
      before do
        create(:transaction, :internal_transaction, amount: 100, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
        create(:transaction, :external_transaction, amount: 150, user: user)
      end
      it 'creates a cashback transaction and credits user wallet' do
        described_class.new(user).level_2_cashback
        expect(user.wallet.point).to eq(60)
        create(:transaction, :external_transaction, amount: 50, user: user)
        described_class.new(user).level_2_cashback
        user.reload
        expect(user.wallet.point).to eq(70)
      end
    end
  end
end

require 'rails_helper'

RSpec.describe LoyaltyService::PreviousYearExpire do
  let(:user) { create(:user) }

  describe '.call' do
    it 'expire the transaction' do
      transaction = create(:transaction, user: user, record_date: 1.year.ago, category: :cashback)
      described_class.call(user)
      expect(transaction.reload.expired?).to be_truthy
    end

    it 'decrements the wallet points' do
      user.wallet.update(point: 100)
      create(:transaction, user: user, record_date: 1.year.ago, amount: 100, category: :cashback)
      described_class.call(user)
      expect(user.wallet.point).to eq(0)
    end
  end
end

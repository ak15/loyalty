require 'rails_helper'

RSpec.describe LoyaltyService::Rule do
  let(:user) { create(:user) }

  describe '#execute' do
    context 'when user has level 1' do
      it 'updates the user tier to gold' do
        user.wallet.update(point: Tier.gold_rule.min_points)
        described_class.new(user).execute
        expect(user.reload.tier_id).to eq(Tier.gold_rule.id)
      end

      it 'updates the user tier to platinum' do
        user.wallet.update(point: Tier.platinum_rule.min_points)
        described_class.new(user).execute
        expect(user.reload.tier_id).to eq(Tier.platinum_rule.id)
      end
    end

    context 'when user has level 2' do
      it 'updates the user tier to gold' do
        user.wallet.update(point: Tier.gold_rule.min_points)
        described_class.new(user).execute
        expect(user.reload.tier_id).to eq(Tier.gold_rule.id)
      end

      it 'updates the user tier to platinum' do
        user.wallet.update(point: Tier.platinum_rule.min_points)
        described_class.new(user).execute
        expect(user.reload.tier_id).to eq(Tier.platinum_rule.id)
      end
    end
  end
end

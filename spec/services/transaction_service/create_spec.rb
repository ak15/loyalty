require 'rails_helper'

RSpec.describe TransactionService::Create do
  describe '.call' do
    let(:user) { create(:user) }
    let(:params) { { user_id: user.id, amount: 100, record_date: Date.today, category: :external } }

    context 'when transaction is valid' do
      it 'creates a transaction' do
        expect do
          described_class.call(params)
          # cashback credited
        end.to change(Transaction, :count).by(2)
      end

      it 'creates a cashback point allocation' do
        expect_any_instance_of(PointAllocationService::Cashback).to receive(:execute)
        described_class.call(params)
      end

      it 'creates a reward credit point allocation' do
        expect_any_instance_of(RewardCreditService::Reward).to receive(:execute)
        described_class.call(params)
      end
    end

    context 'when transaction is invalid' do
      it 'does not create a transaction' do
        expect do
          described_class.call(params.merge(amount: nil))
        end.not_to change(Transaction, :count)
      end

      it 'does not create a cashback point allocation' do
        expect_any_instance_of(PointAllocationService::Cashback).not_to receive(:execute)
        described_class.call(params.merge(amount: nil))
      end

      it 'does not create a reward credit point allocation' do
        expect_any_instance_of(RewardCreditService::Reward).not_to receive(:execute)
        described_class.call(params.merge(amount: nil))
      end
    end
  end
end

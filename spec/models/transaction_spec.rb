require 'rails_helper'

RSpec.describe Transaction, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:parent).optional }
    it { is_expected.to have_many(:children) }
  end

  describe 'validations' do
    it { should validate_presence_of(:amount) }
    it { should validate_presence_of(:category) }
    it { should validate_presence_of(:entry_type) }

    # context 'when category is cashback' do
    #   subject { build(:transaction, category: 'cashback') }

    #   it { should validate_presence_of(:expire_date) }
    # end

    context 'when category is reward' do
      subject { build(:transaction, category: 'reward') }
    end
  end
end

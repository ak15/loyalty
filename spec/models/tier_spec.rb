require 'rails_helper'

RSpec.describe Tier, type: :model do
  subject { create(:tier) }

  describe 'associations' do
    it { should have_many(:users) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
    it { should validate_presence_of(:min_points) }
  end
end

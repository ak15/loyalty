require 'rails_helper'

RSpec.describe RewardCategory, type: :model do
  subject { create(:reward_category) }

  describe 'associations' do
    # it { should have_many(:rewards) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
  end
end

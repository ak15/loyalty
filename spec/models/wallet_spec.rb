require 'rails_helper'

RSpec.describe Wallet, type: :model do
  describe 'associations' do
    it { should have_one(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:point) }
  end

  describe '#credit_cashback_transaction' do
    let(:user) { create(:user) }
    let(:wallet) { user.wallet }
    let(:cashback) { 100 }

    it 'creates a cashback transaction and increments the wallet point' do
      expect do
        wallet.credit_cashback_transaction(cashback)
      end.to change(Transaction, :count).by(1).and change(wallet, :point).by(cashback)
    end
  end

  describe '#debit_cashback_transaction' do
    let(:user) { create(:user) }
    let(:wallet) { user.wallet }
    let(:cashback) { 100 }

    it 'creates a cashback transaction and decrements the wallet point' do
      expect do
        wallet.debit_cashback_transaction(cashback)
      end.to change(Transaction, :count).by(1).and change(wallet, :point).by(-cashback)
    end
  end
end

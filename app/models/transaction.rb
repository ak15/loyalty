class Transaction < ApplicationRecord
  belongs_to :user
  belongs_to :parent, class_name: 'Transaction', optional: true

  enum category: { cashback: 'Cashback', reward: 'Reward', external: 'External Transaction',
                   internal: 'Internal Transaction' }
  enum entry_type: { debit: 'Debit', credit: 'Credit', ledger: 'Ledger' }

  # NOTE: Active -> Transaction is active can be used for generating cashback ,
  #  Closed -> Transaction is closed can't be used for generating cashback, Expired -> Transaction is expired can't be used for generating cashback
  # Partial Active -> Transaction is partially active can be used for generating cashback
  enum status: { active: 'Active', closed: 'Closed', partial_active: 'Partial Active', expired: 'Expired' }

  has_many :children, class_name: 'Transaction', foreign_key: 'parent_id'

  validates :amount, presence: true
  validates :category, presence: true
  validates :entry_type, presence: true
  validates :record_date, presence: true

  # NOTE: We are assuming reward does not have expire date
  # validates :expire_date, presence: true, if: :expire_condition?

  scope :active_partial_active_ledger, lambda {
                                         where(
                                           status: %i[active partial_active],
                                           category: %i[internal external],
                                           entry_type: :ledger
                                         )
                                           .order(record_date: :asc)
                                       }
  scope :active_ledger, lambda {
                          where(status: :active, entry_type: :ledger, category: %i[internal external])
                            .order(record_date: :asc)
                        }
  scope :active_external_ledger, lambda {
                                   where(status: :active, category: :external, entry_type: :ledger)
                                     .order(record_date: :asc)
                                 }

  scope :active_partial_internal_ledger, lambda {
                                           where(status: %i[active partial_active], category: :internal, entry_type: :ledger)
                                             .order(record_date: :asc)
                                         }
  before_create :set_amount_for_cashback

  # def expire_condition?
  #   cashback?
  # end

  private

  def set_amount_for_cashback
    self.amount_for_cashback = amount
  end
end

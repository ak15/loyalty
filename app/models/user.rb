class User < ApplicationRecord
  # NOTE: Ideally will use email as the unique identifier, but here we are using name for simplicity
  validates :name, presence: true, uniqueness: true
  validates :date_of_birth, presence: true

  enum level: { 'level_1' => 'Level 1', 'level_2' => 'Level 2' }

  belongs_to :wallet
  belongs_to :tier

  has_many :transactions
  has_many :rewards

  after_commit :find_or_create_wallet, on: :create
  after_commit :find_or_create_tier, on: :create

  private

  def find_or_create_wallet
    wallet || Wallet.create(user_id: id)
  end

  def find_or_create_tier
    # NOTE: This can be refactored
    tier || Tier.create(name: 'Standard')
  end
end

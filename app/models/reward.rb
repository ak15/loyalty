class Reward < ApplicationRecord
  belongs_to :user
  # belongs_to :reward_category

  # NOTE:  Active -> Reward can be used actively, Expired -> Reward is expired can't be used
  enum status: { active: 'Active', expired: 'Expired' }
  enum category: { free_coffee: 'Free Coffee',
                   cashback_rebate_5_percent: 'Cashback Rebate 5%',
                   airport_longue_four_tickets: ' 4 * Airport Lounge',
                   free_movie_tickets: 'Free Movie Tickets',
                   birthday_reward_coffee: 'Birthday Reward Coffee',
                  quarterly_bonus_milestone: 'Quarterly Bonus Milestone' }
end

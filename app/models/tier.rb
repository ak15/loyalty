class Tier < ApplicationRecord
  has_many :users

  validates :name, presence: true, uniqueness: true
  validates :min_points, presence: true

  class << self
    def gold_rule
      tier = find_or_initialize_by(name: 'Gold')
      tier.min_points = 1000
      tier.save!
      tier
    end

    def platinum_rule
      tier = find_or_initialize_by(name: 'Platinum')
      tier.min_points = 5000
      tier.save!
      tier
    end
  end
end

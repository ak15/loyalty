class Wallet < ApplicationRecord
  has_one :user

  validates :point, presence: true

  def credit_cashback_transaction(cashback)
    transaction = Transaction.create!(
      category: :cashback,
      entry_type: :credit,
      amount: cashback,
      user: user,
      record_date: Date.today,
      status: :active
    )
    increment!(:point, cashback)
    transaction
  end

  def debit_cashback_transaction(cashback)
    transaction = Transaction.create!(
      category: :cashback,
      entry_type: :debit,
      amount: cashback,
      user: user,
      record_date: Date.today,
      status: :active
    )
    decrement!(:point, cashback)
    transaction
  end
end

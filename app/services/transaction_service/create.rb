module TransactionService
  class Create
    class << self
      def call(params)
        # NOTE: Assuming params are whitelisted from controller
        Transaction.transaction do
          transaction = Transaction.new(params)
          transaction.save!
          PointAllocationService::Cashback.new(transaction.user).execute
          RewardCreditService::Reward.new(transaction.user).execute
          LoyaltyService::Rule.new(transaction.user).execute
          { success: true, transaction: transaction }
        end
      rescue ActiveRecord::RecordInvalid => e
        puts "Record invalid: #{e.record.errors.full_messages.join(', ')}"
        { success: false, errors: e.record.errors.full_messages }
      end
    end
  end
end

module RewardCreditService
  class Reward
    LEVEL_1_REWARD_CASHBACK_CHECK = 100
    FREE_TICKET_REWARD_AMOUNT_CHECK = 1000
    CASHBACK_REBATE_5_PERC_AMOUNT_CHECK = 100
    CASHBACK_REBATE_5_PERC_COUNT_CHECK = 10
    FREE_TICKET_REWARD_DAYS_CHECK = 60
    BONUS_MILESTONE_REWARD_POINT = 100
    BONUS_MILESTONE_REWARD_AMOUNT_CHECK = 2000

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def execute
      send("#{user.level}_reward")
    end

    def level_1_reward
      # NOTE: this can also run in scheduler
      reward = user.rewards.where(created_at: Date.today.beginning_of_month..Date.today.end_of_month,
                                  category: :free_coffee)
      # NOTE: already rewarded for current month

      return if reward.present?

      amount = user.transactions.cashback.where(record_date: Date.today.beginning_of_month..Date.today.end_of_month)
                   .sum(:amount)

      return unless amount >= LEVEL_1_REWARD_CASHBACK_CHECK

      reward = user.rewards.create(category: :free_coffee, count: 1)
      { success: true, reward: reward }
    end

    def level_2_reward
      birthday_month_reward
      cashback_rebate_5_percent_reward
      free_movie_tickets_reward
      quaterly_reward
      gold_tier_reward
    end

    private

    def quaterly_reward
      return unless user.level_2?

      quarter = Date.today.beginning_of_quarter..Date.today.end_of_quarter
      reward = user.rewards.where(created_at: quarter, category: :quarterly_bonus_milestone)
      # NOTE: already rewarded for current quarter
      return if reward.present?

      amount = user.transactions.where(category: %i[internal external], record_date: quarter).sum(:amount)
      return unless amount >= BONUS_MILESTONE_REWARD_AMOUNT_CHECK

      reward = user.rewards.create(category: :quarterly_bonus_milestone, count: 1)
      user.wallet.credit_cashback_transaction(BONUS_MILESTONE_REWARD_POINT)

      { success: true, reward: reward }
    end

    def free_movie_tickets_reward
      return unless user.level_2?

      reward = user.rewards.where(category: :free_movie_tickets)

      return if reward.present?

      first_transaction_date = user.transactions.order(:record_date).first&.record_date
      return if first_transaction_date.blank?

      amount = user.transactions.where(category: %i[interna external],
                                       record_date: (first_transaction_date..(first_transaction_date + FREE_TICKET_REWARD_DAYS_CHECK.days)))
                   .sum(:amount)
      return if amount < FREE_TICKET_REWARD_AMOUNT_CHECK

      reward = user.rewards.create(category: :free_movie_tickets, count: 1)
      { success: true, reward: reward }
    end

    def cashback_rebate_5_percent_reward
      return unless user.level_2?

      reward = user.rewards.where(category: :cashback_rebate_5_percent)

      return if reward.present?

      transactions_count = user.transactions.where(category: %i[internal external])
                               .where('amount > ?', CASHBACK_REBATE_5_PERC_AMOUNT_CHECK).count
      return unless transactions_count >= CASHBACK_REBATE_5_PERC_COUNT_CHECK

      reward = user.rewards.create(category: :cashback_rebate_5_percent, count: 1)
      { success: true, reward: reward }
    end

    def birthday_month_reward
      return unless user.date_of_birth.month == Date.today.month && user.level_2?

      reward = user.rewards.where(created_at: Date.today.beginning_of_month..Date.today.end_of_month,
                                  category: :birthday_reward_coffee)
      # NOTE: already rewarded for current month

      return if reward.present?

      reward = user.rewards.create(category: :birthday_reward_coffee, count: 1)
      { success: true, reward: reward }
    end

    def gold_tier_reward
      return unless user.level_2? && user.tier_id == Tier.gold_rule.id

      reward = user.rewards.where(category: :airport_longue_four_tickets)

      return if reward.present?

      reward = user.rewards.create(category: :airport_longue_four_tickets, count: 1)
      { success: true, reward: reward }
    end
  end
end

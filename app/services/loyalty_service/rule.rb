module LoyaltyService
  class Rule
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def execute
      send("#{user.level}_rule")
    end

    def level_1_rule
      if user.wallet.point >= Tier.platinum_rule.min_points
        user.update(tier_id: Tier.platinum_rule.id)
      elsif user.wallet.point >= Tier.gold_rule.min_points
        user.update(tier_id: Tier.gold_rule.id)
      end
    end

    def level_2_rule
      PreviousYearExpire.call(user)
      caclulate_loyalty_tier
    end

    def caclulate_loyalty_tier
      current_quarter = Date.today.beginning_of_quarter
      previous_quarter = 3.months.ago.beginning_of_quarter
      return unless previous_quarter.year == current_quarter.year

      current_quarter_max = user.transactions.cashback(record_date: current_quarter...current_quarter.end_of_quarter)
                                .maximum(:amount)
      previous_quarter_max = user.transactions.cashback(record_date: previous_quarter...previous_quarter.end_of_quarter)
                                 .maximum(:amount)

      total_amount = current_quarter_max + previous_quarter_max
      if total_amount >= Tier.gold_rule.min_points
        user.update(tier_id: Tier.Tier.gold_rule.id)
      elsif total_amount >= Tier.platinum_rule.min_points
        user.update(tier_id: Tier.Tier.platinum_rule.id)
      end
    end
  end
end

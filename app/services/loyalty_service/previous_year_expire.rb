module LoyaltyService
  class PreviousYearExpire
    class << self
      def call(user)
        previous_year = 1.year.ago.beginning_of_month
        end_year = previous_year.end_of_year
        user.transactions.cashback.where(record_date: previous_year..end_year).find_each do |transact|
          ActiveRecord::Base.transaction do
            transact.expired!
            user.wallet.debit_cashback_transaction(transact.amount)
          end
        end
      end
    end
  end
end

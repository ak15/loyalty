module PointAllocationService
  class Cashback
    STANDARD = 10
    SPEND_CHECK_AMOUNT = 100

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def execute
      send("#{user.level}_cashback")
    end

    def level_1_cashback
      sum = user.transactions.active_partial_active_ledger.sum(:amount_for_cashback)
      return if sum < SPEND_CHECK_AMOUNT

      cashback = (sum.to_i / SPEND_CHECK_AMOUNT) * STANDARD
      diff = sum - (cashback * STANDARD)

      ledger(cashback, diff)
    end

    def level_2_cashback
      # NOTE: Foreign transactions cashback
      cashback = user.transactions.active_external_ledger.count * STANDARD
      # NOTE: Internal transactions cashback, its not mentioned but I am assuming it, can be removed if not needed
      sum_from_internal = user.transactions.active_partial_internal_ledger.sum(:amount_for_cashback)

      cashback += (sum_from_internal.to_i / SPEND_CHECK_AMOUNT) * STANDARD

      diff = sum_from_internal - (cashback * STANDARD)
      ledger(cashback, diff)
    end

    private

    def ledger(cashback, diff)
      ActiveRecord::Base.transaction do
        transaction = user.wallet.credit_cashback_transaction(cashback)
        send("#{user.level}_transaction_entry", diff, transaction)
      end
    end

    def level_1_transaction_entry(diff, cashback_transaction)
      transaction_based_ledger(user.transactions.active_partial_active_ledger, cashback_transaction, diff)
    end

    def level_2_transaction_entry(diff, cashback_transaction)
      user.transactions.active_external_ledger.update_all(status: :closed, amount_for_cashback: 0,
                                                          parent_id: cashback_transaction.id)
      transaction_based_ledger(user.transactions.active_partial_internal_ledger, cashback_transaction, diff)
    end

    def transaction_based_ledger(transactions, cashback_transaction, diff)
      if diff.positive?

        transactions.find_each do |transaction|
          diff = (diff - transaction.amount_for_cashback)
          status = diff.zero? ? :closed : :partial_active
          amount_for_cashback = diff.abs

          transaction.update!(amount_for_cashback: amount_for_cashback, status: status,
                              parent_id: cashback_transaction.id)

          break if diff.negative? || diff.zero?
        end
      end
      # All remaining transactions which are in active will be closed
      transactions.update_all(status: :closed, amount_for_cashback: 0,
                              parent_id: cashback_transaction.id)
    end
  end
end
